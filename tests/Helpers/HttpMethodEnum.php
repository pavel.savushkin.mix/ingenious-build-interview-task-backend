<?php

namespace Tests\Helpers;

enum HttpMethodEnum: string
{
    case GET = 'GET';
    case POST = 'POST';
    case PATCH = 'PATCH';
    case DELETE = 'DELETE';
}
