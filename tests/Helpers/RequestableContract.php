<?php

namespace Tests\Helpers;

use Illuminate\Testing\TestResponse;

interface RequestableContract
{
    /**
     * Returns a route name for the request.
     *
     * @return string
     */
    public function getRouteName(): string;

    /**
     * Returns a method of the route.
     *
     * @return HttpMethodEnum
     */
    public function getMethod(): HttpMethodEnum;

    /**
     * Makes request to specified route name by specified method.
     *
     * @param array $parameters
     * @param array $data
     * @param array $headers
     * @param int $options
     *
     * @return TestResponse
     */
    public function makeRequest(array $parameters = [], array $data = [], array $headers = [], int $options = 0): TestResponse;
}
