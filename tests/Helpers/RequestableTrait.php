<?php

namespace Tests\Helpers;

use Illuminate\Testing\TestResponse;

trait RequestableTrait
{
    /**
     * {@inheritDoc}
     */
    public function makeRequest(array $parameters = [], array $data = [], array $headers = [], int $options = 0): TestResponse
    {
        $uri = route($this->getRouteName(), $parameters);

        return $this->json($this->getMethod()->value, $uri, $data, $headers, $options);
    }
}
