<?php

namespace App\Domain\Presenter;

interface PresenterInterface
{
    /**
     * Converts the provided DTO to the array of the fields, that should be returned to client.
     *
     * @return array
     */
    public function present($entity): array;

    /**
     * Converts all the provided DTOs to the fields to show to client.
     *
     * @param array $entities
     *
     * @return array
     */
    public function presentAll(array $entities): array;
}
