<?php

namespace App\Domain\Presenter;

abstract class BasePresenter implements PresenterInterface
{
    /**
     * {@inheritDoc}
     */
    public function presentAll(array $entities): array
    {
        $result = [];
        foreach ($entities as $entity) {
            $result[] = $this->present($entity);
        }

        return $result;
    }
}
