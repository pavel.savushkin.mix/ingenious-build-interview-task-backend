<?php

namespace App\Domain\Transformer;

use Illuminate\Support\Collection;

abstract class BaseTransformer implements TransformerInterface
{
    /**
     * {@inheritDoc}
     */
    public function transformAll(Collection $models): array
    {
        $result = [];
        foreach ($models as $model) {
            $result[] = $this->transform($model);
        }

        return $result;
    }
}
