<?php

namespace App\Domain\Transformer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface TransformerInterface
{
    /**
     * Transforms the model to their DTO.
     *
     * @param Model $model
     *
     * @return mixed
     */
    public function transform(Model $model);

    /**
     * Transforms all provided models.
     *
     * @param Collection $models
     *
     * @return array
     */
    public function transformAll(Collection $models): array;
}
