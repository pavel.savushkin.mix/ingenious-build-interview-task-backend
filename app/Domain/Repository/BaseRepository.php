<?php

namespace App\Domain\Repository;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function exists(string $id): bool
    {
        $model = $this->getModel();

        return $model->where($model->getKeyName(), $id)->exists();
    }
}
