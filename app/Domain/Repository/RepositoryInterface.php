<?php

namespace App\Domain\Repository;

use App\Domain\Transformer\TransformerInterface;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Returns a transformer for the specific repository.
     *
     * @return TransformerInterface
     */
    public function getTransformer(): TransformerInterface;

    /**
     * Returns a namespace of the model.
     *
     * @return Model
     */
    public function getModel(): Model;

    /**
     * Determines if the specified ID exist in DB.
     *
     * @param string $id
     *
     * @return bool
     */
    public function exists(string $id): bool;
}
