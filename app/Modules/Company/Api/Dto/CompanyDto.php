<?php

namespace App\Modules\Company\Api\Dto;

use Carbon\Carbon;
use Ramsey\Uuid\UuidInterface;

final readonly class CompanyDto
{
    /**
     * @param UuidInterface $id
     * @param string $name
     * @param string $street
     * @param string $city
     * @param string $zip
     * @param string $phone
     * @param string $email
     * @param Carbon $created_at
     * @param Carbon $updated_at
     */
    public function __construct(
        public UuidInterface $id,
        public string        $name,
        public string        $street,
        public string        $city,
        public string        $zip,
        public string        $phone,
        public string        $email,
        public Carbon        $created_at,
        public Carbon        $updated_at,
    )
    {
    }
}
