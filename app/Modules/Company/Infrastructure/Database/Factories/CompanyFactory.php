<?php

namespace App\Modules\Company\Infrastructure\Database\Factories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Company\Domain\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Company>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id' => Str::orderedUuid()->toString(),
            'name' => fake()->company(),
            'street' => fake()->streetAddress(),
            'city' => fake()->city(),
            'zip' => fake()->postcode(),
            'phone' => fake()->phoneNumber(),
            'email' => fake()->unique()->companyEmail(),
        ];
    }

    /**
     * @return static
     */
    public function draft()
    {
        return $this->state(fn(array $attributes) => [
            'status' => StatusEnum::DRAFT->value,
        ]);
    }

    /**
     * @return static
     */
    public function approved()
    {
        return $this->state(fn(array $attributes) => [
            'approved' => StatusEnum::APPROVED->value,
        ]);
    }

    /**
     * @return static
     */
    public function rejected()
    {
        return $this->state(fn(array $attributes) => [
            'rejected' => StatusEnum::REJECTED->value,
        ]);
    }
}
