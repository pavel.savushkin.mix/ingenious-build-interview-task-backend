<?php

namespace App\Modules\Company\Infrastructure\Providers;

use App\Modules\Company\Domain\Presenters\CompanyPresenter;
use App\Modules\Company\Domain\Presenters\CompanyPresenterInterface;
use App\Modules\Company\Domain\Transformers\CompanyTransformer;
use App\Modules\Company\Domain\Transformers\CompanyTransformerInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class CompaniesServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(CompanyPresenterInterface::class, CompanyPresenter::class);
        $this->app->scoped(CompanyTransformerInterface::class, CompanyTransformer::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            CompanyPresenterInterface::class,
            CompanyTransformerInterface::class,
        ];
    }
}
