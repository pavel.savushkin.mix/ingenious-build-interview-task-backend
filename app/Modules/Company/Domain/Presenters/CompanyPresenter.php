<?php

namespace App\Modules\Company\Domain\Presenters;

use App\Domain\Presenter\BasePresenter;
use App\Modules\Company\Api\Dto\CompanyDto;

class CompanyPresenter extends BasePresenter implements CompanyPresenterInterface
{
    /**
     * @param CompanyDto $entity
     * @return array
     */
    public function present($entity): array
    {
        return [
            'name' => $entity->name,
            'street' => $entity->street,
            'city' => $entity->city,
            'zip' => $entity->zip,
            'phone' => $entity->phone,
            'email' => $entity->email,
        ];
    }
}
