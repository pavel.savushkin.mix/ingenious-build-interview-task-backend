<?php

namespace App\Modules\Company\Domain\Transformers;

use App\Domain\Transformer\BaseTransformer;
use App\Modules\Company\Api\Dto\CompanyDto;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

class CompanyTransformer extends BaseTransformer implements CompanyTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform(Model $model)
    {
        return new CompanyDto(
            id: new LazyUuidFromString($model->id),
            name: $model->name,
            street: $model->street,
            city: $model->city,
            zip: $model->zip,
            phone: $model->phone,
            email: $model->email,
            created_at: $model->created_at,
            updated_at: $model->updated_at,
        );
    }
}
