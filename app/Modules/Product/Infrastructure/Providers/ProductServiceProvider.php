<?php

namespace App\Modules\Product\Infrastructure\Providers;

use App\Modules\Product\Domain\Presenters\ProductPresenter;
use App\Modules\Product\Domain\Presenters\ProductPresenterInterface;
use App\Modules\Product\Domain\Transformers\ProductTransformer;
use App\Modules\Product\Domain\Transformers\ProductTransformerInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(ProductPresenterInterface::class, ProductPresenter::class);
        $this->app->scoped(ProductTransformerInterface::class, ProductTransformer::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            ProductPresenterInterface::class,
            ProductTransformerInterface::class,
        ];
    }
}
