<?php

namespace App\Modules\Product\Infrastructure\Database\Factories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Product\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id' => Str::orderedUuid()->toString(),
            'name' => fake()->word(),
            'price' => fake()->numberBetween(100, 100000),
            'currency' => 'USD',
        ];
    }

    /**
     * @return static
     */
    public function draft()
    {
        return $this->state(fn(array $attributes) => [
            'status' => StatusEnum::DRAFT->value,
        ]);
    }

    /**
     * @return static
     */
    public function approved()
    {
        return $this->state(fn(array $attributes) => [
            'approved' => StatusEnum::APPROVED->value,
        ]);
    }

    /**
     * @return static
     */
    public function rejected()
    {
        return $this->state(fn(array $attributes) => [
            'rejected' => StatusEnum::REJECTED->value,
        ]);
    }
}
