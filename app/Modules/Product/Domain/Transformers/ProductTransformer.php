<?php

namespace App\Modules\Product\Domain\Transformers;

use App\Domain\Transformer\BaseTransformer;
use App\Modules\Product\Api\Dto\ProductDto;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

class ProductTransformer extends BaseTransformer implements ProductTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform(Model $model)
    {
        return new ProductDto(
            id: new LazyUuidFromString($model->id),
            name: $model->name,
            price: $model->price,
            currency: $model->currency,
            quantity: (int)$model->pivot->quantity,
            createdAt: $model->created_at,
            updatedAt: $model->updated_at,
        );
    }
}
