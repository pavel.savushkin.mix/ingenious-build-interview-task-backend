<?php

namespace App\Modules\Product\Domain\Transformers;

use App\Domain\Transformer\TransformerInterface;

interface ProductTransformerInterface extends TransformerInterface
{

}
