<?php

namespace App\Modules\Product\Domain\Presenters;

use App\Domain\Presenter\BasePresenter;
use App\Modules\Product\Api\Dto\ProductDto;

class ProductPresenter extends BasePresenter implements ProductPresenterInterface
{
    /**
     * @param ProductDto $entity
     *
     * @return array
     */
    public function present($entity): array
    {
        return [
            'name' => $entity->name,
            'quantity' => $entity->quantity,
            'unit_price' => $entity->price,
            'total' => $entity->price * $entity->quantity,
        ];
    }
}
