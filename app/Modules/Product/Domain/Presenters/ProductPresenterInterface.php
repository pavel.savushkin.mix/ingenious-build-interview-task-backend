<?php

namespace App\Modules\Product\Domain\Presenters;

use App\Domain\Presenter\PresenterInterface;

interface ProductPresenterInterface extends PresenterInterface
{

}
