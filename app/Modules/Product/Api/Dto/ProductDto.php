<?php

namespace App\Modules\Product\Api\Dto;

use Carbon\Carbon;
use Ramsey\Uuid\UuidInterface;

final readonly class ProductDto
{
    /**
     * @param UuidInterface $id
     * @param string $name
     * @param int $price
     * @param string $currency
     * @param Carbon $created_at
     * @param Carbon $updated_at
     */
    public function __construct(
        public UuidInterface $id,
        public string        $name,
        public int           $price,
        public string        $currency,
        public int           $quantity,
        public Carbon        $createdAt,
        public Carbon        $updatedAt,
    )
    {
    }
}
