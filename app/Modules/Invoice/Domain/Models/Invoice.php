<?php

namespace App\Modules\Invoice\Domain\Models;

use App\Domain\Enums\StatusEnum;
use App\Modules\Company\Domain\Models\Company;
use App\Modules\Product\Domain\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Invoice extends Model
{
    use HasFactory;

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'number',
        'date',
        'due_date',
        'company_id',
        'status',
        'created_at',
        'updated_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:Y-m-d',
        'due_date' => 'datetime:Y-m-d',
        'status' => StatusEnum::class,
    ];

    /**
     * Returns a company of the current invoice.
     *
     * @return BelongsTo
     */
    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Returns a billed company of the current invoice.
     *
     * @return BelongsTo
     */
    public function billed_company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'billed_company_id');
    }

    /**
     * Returns products of the current invoice.
     *
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, InvoiceProductLine::class, 'invoice_id', 'product_id')
            ->withPivot(['quantity']);
    }
}
