<?php

namespace App\Modules\Invoice\Domain\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class InvoiceProductLine extends Pivot
{
    /**
     * @var string
     */
    protected $table = 'invoice_product_lines';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string[]
     */
    protected $fillable = [
        'id',
        'invoice_id',
        'product_id',
        'quantity',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'quantity' => 'integer',
    ];
}
