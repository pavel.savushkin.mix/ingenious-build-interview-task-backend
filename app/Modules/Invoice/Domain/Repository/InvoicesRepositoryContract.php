<?php

namespace App\Modules\Invoice\Domain\Repository;

use App\Domain\Enums\StatusEnum;
use App\Domain\Repository\RepositoryInterface;
use App\Modules\Invoice\Api\Dto\InvoiceDto;

interface InvoicesRepositoryContract extends RepositoryInterface
{
    /**
     * @param string $id
     *
     * @return InvoiceDto
     */
    public function get(string $id): InvoiceDto;

    /**
     * Determines if the status of invoice with provided ID can be updated.
     *
     * @param string $id
     *
     * @return bool
     */
    public function isStatusUpdatable(string $id): bool;

    /**
     * Updates the status of the invoice with the specified ID.
     *
     * @param string $id
     * @param StatusEnum $status
     *
     * @return bool
     */
    public function updateStatus(string $id, StatusEnum $status): bool;
}
