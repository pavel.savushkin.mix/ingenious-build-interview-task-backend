<?php

namespace App\Modules\Invoice\Domain\Repository;

use App\Domain\Enums\StatusEnum;
use App\Domain\Repository\BaseRepository;
use App\Domain\Transformer\TransformerInterface;
use App\Modules\Invoice\Api\Dto\InvoiceDto;
use App\Modules\Invoice\Domain\Models\Invoice;
use App\Modules\Invoice\Domain\Transformers\InvoiceTransformerInterface;
use Illuminate\Database\Eloquent\Model;

class EloquentInvoicesRepository extends BaseRepository implements InvoicesRepositoryContract
{
    /**
     * {@inheritDoc}
     */
    public function get(string $id): InvoiceDto
    {
        /** @var Invoice $invoice */
        $invoice = Invoice::query()
            ->with([
                'company',
                'billed_company',
                'products',
            ])
            ->findOrFail($id);

        return $this->getTransformer()->transform($invoice);
    }

    /**
     * @inheritDoc
     */
    public function getTransformer(): TransformerInterface
    {
        return app()->make(InvoiceTransformerInterface::class);
    }

    /**
     * @inheritDoc
     */
    public function isStatusUpdatable(string $id): bool
    {
        return Invoice::query()
            ->where('id', $id)
            ->where('status', StatusEnum::DRAFT->value)
            ->exists();
    }

    /**
     * @inheritDoc
     */
    public function getModel(): Model
    {
        return new Invoice();
    }

    /**
     * @inheritDoc
     */
    public function updateStatus(string $id, StatusEnum $status): bool
    {
        return Invoice::query()
            ->where('id', $id)
            ->update([
                'status' => $status,
            ]) > 0;
    }
}
