<?php

namespace App\Modules\Invoice\Domain\Transformers;

use App\Domain\Transformer\BaseTransformer;
use App\Modules\Company\Domain\Transformers\CompanyTransformerInterface;
use App\Modules\Invoice\Api\Dto\InvoiceDto;
use App\Modules\Product\Domain\Transformers\ProductTransformerInterface;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Lazy\LazyUuidFromString;

class InvoiceTransformer extends BaseTransformer implements InvoiceTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform(Model $model)
    {
        /** @var CompanyTransformerInterface $companyTransformer */
        $companyTransformer = app()->make(CompanyTransformerInterface::class);
        /** @var ProductTransformerInterface $productTransformer */
        $productTransformer = app()->make(ProductTransformerInterface::class);

        return new InvoiceDto(
            id: new LazyUuidFromString($model->id),
            number: $model->number,
            date: $model->date,
            dueDate: $model->due_date,
            company: $companyTransformer->transform($model->company),
            billedCompany: $companyTransformer->transform($model->billed_company),
            products: $productTransformer->transformAll($model->products),
            status: $model->status,
            createdAt: $model->created_at,
            updatedAt: $model->updated_at,
        );
    }
}
