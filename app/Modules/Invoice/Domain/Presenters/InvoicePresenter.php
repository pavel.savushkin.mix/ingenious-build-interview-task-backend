<?php

namespace App\Modules\Invoice\Domain\Presenters;

use App\Domain\Presenter\BasePresenter;
use App\Modules\Company\Domain\Presenters\CompanyPresenterInterface;
use App\Modules\Invoice\Api\Dto\InvoiceDto;
use App\Modules\Product\Domain\Presenters\ProductPresenterInterface;

class InvoicePresenter extends BasePresenter implements InvoicePresenterInterface
{
    /**
     * @param InvoiceDto $entity
     * @return array
     */
    public function present($entity): array
    {
        /** @var CompanyPresenterInterface $companyPresenter */
        $companyPresenter = app()->make(CompanyPresenterInterface::class);
        /** @var ProductPresenterInterface $productPresenter */
        $productPresenter = app()->make(ProductPresenterInterface::class);

        $products = $productPresenter->presentAll($entity->products);
        $totalPrice = collect($products)->sum('total');

        return [
            'number' => $entity->number,
            'date' => $entity->date->format('Y-m-d'),
            'due_date' => $entity->dueDate->format('Y-m-d'),
            'company' => $companyPresenter->present($entity->company),
            'billed_company' => $companyPresenter->present($entity->billedCompany),
            'products' => $products,
            'total_price' => $totalPrice,
        ];
    }
}
