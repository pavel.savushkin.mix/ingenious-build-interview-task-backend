<?php

namespace App\Modules\Invoice\Domain\Presenters;

use App\Domain\Presenter\PresenterInterface;

interface InvoicePresenterInterface extends PresenterInterface
{

}
