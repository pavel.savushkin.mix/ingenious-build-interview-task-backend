<?php

namespace App\Modules\Invoice\Api\Dto;

use App\Domain\Enums\StatusEnum;
use App\Modules\Company\Api\Dto\CompanyDto;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\UuidInterface;

final readonly class InvoiceDto
{
    /**
     * @param UuidInterface $id
     * @param string $number
     * @param Carbon $date
     * @param Carbon $dueDate
     * @param CompanyDto $company
     * @param CompanyDto $billedCompany
     * @param array $products
     * @param StatusEnum $status
     * @param Carbon $createdAt
     * @param Carbon $updatedAt
     */
    public function __construct(
        public UuidInterface $id,
        public string        $number,
        public Carbon        $date,
        public Carbon        $dueDate,
        public CompanyDto    $company,
        public CompanyDto    $billedCompany,
        public array         $products,
        public StatusEnum    $status,
        public Carbon        $createdAt,
        public Carbon        $updatedAt,
    )
    {
    }
}
