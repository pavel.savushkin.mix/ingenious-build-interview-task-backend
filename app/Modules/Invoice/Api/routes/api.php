<?php

use App\Modules\Invoice\Infrastructure\Http\Controllers\InvoiceController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => '{invoice}',
], function () {

    Route::get('', [
        'as' => 'show',
        'uses' => InvoiceController::class . '@show',
    ])->whereUuid('invoice');

    Route::patch('approve', [
        'as' => 'approve',
        'uses' => InvoiceController::class . '@approve',
    ])->whereUuid('invoice');

    Route::patch('reject', [
        'as' => 'reject',
        'uses' => InvoiceController::class . '@reject',
    ])->whereUuid('invoice');

});
//->whereUuid([
//    // TODO: investigate why it passes non-uuid values to controller;
//    //  workaround: apply whereUuid to specific route instead of the group;
//    //  how to test:
//    //      `dd($this->invoice)` in PatchInvoiceStatusRequest.php
//    //      -> php artisan test --filter=PatchInvoiceStatusTest
//    //      -> see non-uuid-value is passed to request
//    'invoice',
//]);
