<?php

declare(strict_types=1);

namespace App\Modules\Invoice\Infrastructure\Providers;

use App\Modules\Invoice\Domain\Presenters\InvoicePresenter;
use App\Modules\Invoice\Domain\Presenters\InvoicePresenterInterface;
use App\Modules\Invoice\Domain\Repository\EloquentInvoicesRepository;
use App\Modules\Invoice\Domain\Repository\InvoicesRepositoryContract;
use App\Modules\Invoice\Domain\Transformers\InvoiceTransformer;
use App\Modules\Invoice\Domain\Transformers\InvoiceTransformerInterface;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class InvoicesServiceProvider extends RouteServiceProvider
{
    /**
     * Keeps the module's path.
     */
    private const MODULE_PATH = 'app/Modules/Invoice';

    public function register(): void
    {
        $this->app->scoped(InvoicesRepositoryContract::class, EloquentInvoicesRepository::class);
        $this->app->scoped(InvoicePresenterInterface::class, InvoicePresenter::class);
        $this->app->scoped(InvoiceTransformerInterface::class, InvoiceTransformer::class);

        parent::register();
    }

    public function boot(): void
    {
        $this->routes(function (): void {
            Route::middleware('api')
                ->prefix('api/invoices')
                ->as('invoices.')
                ->group(base_path(self::MODULE_PATH . '/Api/routes/api.php'));
        });
    }
}
