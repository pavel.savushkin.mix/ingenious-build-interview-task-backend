<?php

namespace App\Modules\Invoice\Infrastructure\Http\Controllers;

use App\Domain\Enums\StatusEnum;
use App\Infrastructure\Controller;
use App\Modules\Invoice\Domain\Presenters\InvoicePresenterInterface;
use App\Modules\Invoice\Domain\Repository\InvoicesRepositoryContract;
use App\Modules\Invoice\Infrastructure\Http\Requests\ApproveInvoiceStatusRequest;
use App\Modules\Invoice\Infrastructure\Http\Requests\PatchInvoiceStatusRequest;
use App\Modules\Invoice\Infrastructure\Http\Requests\RejectInvoiceStatusRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{
    /**
     * Shows the invoice's details.
     *
     * @param InvoicesRepositoryContract $repository
     * @param InvoicePresenterInterface $presenter
     * @param string $invoiceId
     *
     * @return JsonResponse
     */
    public function show(InvoicesRepositoryContract $repository, InvoicePresenterInterface $presenter, string $invoiceId)
    {
        $entity = $repository->get($invoiceId);

        $result = $presenter->present($entity);

        return response()->json($result);
    }

    /**
     * Approved the provided invoice.
     *
     * @param ApproveInvoiceStatusRequest $request
     * @param InvoicesRepositoryContract $repository
     * @param string $invoiceId
     *
     * @return Response
     */
    public function approve(ApproveInvoiceStatusRequest $request, InvoicesRepositoryContract $repository, string $invoiceId)
    {
        $repository->updateStatus($invoiceId, StatusEnum::APPROVED);

        return response()->noContent();
    }

    /**
     * Rejects the provided invoice.
     *
     * @param RejectInvoiceStatusRequest $request
     * @param InvoicesRepositoryContract $repository
     * @param string $invoiceId
     *
     * @return Response
     */
    public function reject(RejectInvoiceStatusRequest $request, InvoicesRepositoryContract $repository, string $invoiceId)
    {
        $repository->updateStatus($invoiceId, StatusEnum::REJECTED);

        return response()->noContent();
    }
}
