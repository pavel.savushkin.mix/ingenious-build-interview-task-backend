<?php

namespace App\Modules\Invoice\Infrastructure\Http\Requests;

use App\Modules\Invoice\Domain\Repository\InvoicesRepositoryContract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class ApproveInvoiceStatusRequest extends PatchInvoiceStatusRequest
{
}
