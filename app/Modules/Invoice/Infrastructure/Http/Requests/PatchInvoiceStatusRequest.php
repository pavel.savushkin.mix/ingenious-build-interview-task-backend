<?php

namespace App\Modules\Invoice\Infrastructure\Http\Requests;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoice\Domain\Repository\InvoicesRepositoryContract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;

class PatchInvoiceStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var InvoicesRepositoryContract $invoicesRepository */
        $invoicesRepository = app()->make(InvoicesRepositoryContract::class);
        $invoice = $this->invoice;

        abort_if(!$invoicesRepository->exists($invoice), Response::HTTP_NOT_FOUND);

        return $invoicesRepository->isStatusUpdatable($invoice);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
