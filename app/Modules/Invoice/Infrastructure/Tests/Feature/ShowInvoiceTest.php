<?php

namespace App\Modules\Invoice\Infrastructure\Tests\Feature;

use App\Modules\Invoice\Domain\Models\Invoice;
use App\Modules\Product\Domain\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\Helpers\HttpMethodEnum;
use Tests\Helpers\RequestableContract;
use Tests\Helpers\RequestableTrait;
use Tests\TestCase;

class ShowInvoiceTest extends TestCase implements RequestableContract
{
    use RequestableTrait, RefreshDatabase;

    /**
     * @return void
     */
    public function test_non_uuid_throws_not_found()
    {
        $response = $this->makeRequest(['invoice' => 'non-uuid-value']);

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function test_absent_uuid_throws_not_found()
    {
        $response = $this->makeRequest(['invoice' => Str::uuid()->toString()]);

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function test_fetching_exist_invoice_returns_successful_response()
    {
        $invoice = Invoice::factory()->create();
        $invoiceId = $invoice->id;
        $invoiceNumber = $invoice->number;

        $response = $this->makeRequest(['invoice' => $invoiceId]);

        $response->assertOk();
        $response->assertJson(['number' => $invoiceNumber]);
    }

    /**
     * @return void
     */
    public function test_check_if_the_total_price_of_product_calculated_correctly()
    {
        $price = 25;
        $quantity = 4;

        /** @var Invoice $invoice */
        $invoice = Invoice::factory()
            ->hasAttached(Product::factory()->create(['price' => $price]), [
                'id' => Str::orderedUuid()->toString(),
                'quantity' => $quantity,
            ])
            ->create();
        $invoiceNumber = $invoice->number;

        $response = $this->makeRequest(['invoice' => $invoice->id]);

        $response->assertOk();
        $response->assertJson([
            'number' => $invoiceNumber,
            'products' => [
                [
                    'total' => $price * $quantity,
                ],
            ],
        ]);
    }

    /**
     * @return void
     */
    public function test_check_if_the_total_price_of_all_products_calculated_correctly()
    {
        $price = 25;
        $quantity = 4;
        $productsNumber = 5;

        /** @var Invoice $invoice */
        $invoice = Invoice::factory()
            ->hasAttached(Product::factory($productsNumber)->create(['price' => $price]), fn () => [
                'id' => Str::orderedUuid()->toString(),
                'quantity' => $quantity,
            ])
            ->create();
        $invoiceNumber = $invoice->number;

        $response = $this->makeRequest(['invoice' => $invoice->id]);

        $response->assertOk();
        $response->assertJson([
            'number' => $invoiceNumber,
            'total_price' => $price * $quantity * $productsNumber,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getRouteName(): string
    {
        return 'invoices.show';
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): HttpMethodEnum
    {
        return HttpMethodEnum::GET;
    }
}
