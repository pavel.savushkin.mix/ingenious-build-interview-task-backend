<?php

namespace App\Modules\Invoice\Infrastructure\Tests\Feature;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoice\Domain\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\Helpers\HttpMethodEnum;
use Tests\Helpers\RequestableContract;
use Tests\Helpers\RequestableTrait;
use Tests\TestCase;

class RejectInvoiceTest extends TestCase implements RequestableContract
{
    use RequestableTrait, RefreshDatabase;

    /**
     * @return void
     */
    public function test_non_uuid_throws_not_found()
    {
        $response = $this->makeRequest(['invoice' => 'non-uuid-value']);

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function test_absent_uuid_throws_not_found()
    {
        $response = $this->makeRequest(['invoice' => Str::uuid()->toString()]);

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function test_already_processed_invoice_should_be_rejected()
    {
        $processedStatuses = [
            StatusEnum::APPROVED->value,
            StatusEnum::REJECTED->value,
        ];

        foreach ($processedStatuses as $processedStatus) {
            $invoice = Invoice::factory()->create([
                'status' => $processedStatus,
            ]);
            $invoiceId = $invoice->id;

            $response = $this->makeRequest(['invoice' => $invoiceId]);
            $response->assertForbidden();
        }
    }

    /**
     * @return void
     */
    public function test_successfully_patched_invoice_status()
    {
        $invoice = Invoice::factory()->create([
            'status' => StatusEnum::DRAFT->value,
        ]);
        $invoiceId = $invoice->id;

        $response = $this->makeRequest(['invoice' => $invoiceId]);
        $response->assertSuccessful();
        $this->assertDatabaseHas('invoices', [
            'id' => $invoiceId,
            'status' => StatusEnum::REJECTED->value,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getRouteName(): string
    {
        return 'invoices.reject';
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): HttpMethodEnum
    {
        return HttpMethodEnum::PATCH;
    }
}
