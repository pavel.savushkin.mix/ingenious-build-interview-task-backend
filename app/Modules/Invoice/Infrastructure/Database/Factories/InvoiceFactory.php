<?php

namespace App\Modules\Invoice\Infrastructure\Database\Factories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Company\Domain\Models\Company;
use App\Modules\Invoice\Domain\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Invoice>
 */
class InvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id' => Str::orderedUuid()->toString(),
            'number' => Str::uuid()->toString(),
            'date' => fake()->date(),
            'due_date' => fake()->date(),
            'company_id' => Company::factory(),
            'billed_company_id' => Company::factory(),
            'status' => fake()->randomElement(StatusEnum::cases()),
        ];
    }

    /**
     * @return static
     */
    public function draft()
    {
        return $this->state(fn(array $attributes) => [
            'status' => StatusEnum::DRAFT->value,
        ]);
    }

    /**
     * @return static
     */
    public function approved()
    {
        return $this->state(fn(array $attributes) => [
            'approved' => StatusEnum::APPROVED->value,
        ]);
    }

    /**
     * @return static
     */
    public function rejected()
    {
        return $this->state(fn(array $attributes) => [
            'rejected' => StatusEnum::REJECTED->value,
        ]);
    }
}
