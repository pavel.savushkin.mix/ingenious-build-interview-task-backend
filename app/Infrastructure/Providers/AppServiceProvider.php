<?php

namespace App\Infrastructure\Providers;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setupFactories();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    protected function setupFactories()
    {
        // Sets a custom namespace for factories
        Factory::guessFactoryNamesUsing(function (string $modelFullName) {
            $modelBasename = Str::replaceLast('Factory', '', class_basename($modelFullName));

            return "App\Modules\\${modelBasename}\Infrastructure\Database\Factories\\${modelBasename}Factory";
        });

        // Sets a custom namespace for models, that are used by factories
        Factory::guessModelNamesUsing(function (Factory $factory) {
            $factoryBasename = Str::replaceLast('Factory', '', class_basename($factory));

            return "App\Modules\\${factoryBasename}\Domain\Models\\${factoryBasename}";
        });
    }
}
